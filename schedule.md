---
layout: page
title: Programação
permalink: /schedule/
---

Temos uma proposta que mistura palestras e ofinas para o Debian Day, confira:

## Manhã

* Abertura: O que é o Debian
* Instalando um GNU Debian 100% livre
* Conhecendo o Debian Bug Tracking System (BTS)
* Opções para participar da comunidade

## Tarde

* O que é uma chave GPG e assinaturas
* Precisamos discutir a liberdade dos usuário
* Lightning Talks (palestras de até 10 minutos)
